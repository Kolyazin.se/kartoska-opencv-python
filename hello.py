import numpy as np
import cv2
import imutils


image = cv2.imread("cALND.png")
gray = cv2.imread("kartoshka.jpg")

#gray = cv2.cvtColor(gray, cv2.COLOR_BGR2HSV)


#gray = cv2.imread("cALND.png")
gray = cv2.cvtColor(gray, cv2.COLOR_RGB2GRAY)
blur = cv2.medianBlur(gray, 11)
cv2.imshow('blur', blur)
thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5,5))
opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=3)
cnts = cv2.findContours(opening, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0] if len(cnts) == 2 else cnts[1]
for c in cnts:
    peri = cv2.arcLength(c, True)
    approx = cv2.approxPolyDP(c, 0.04 * peri, True)
    area = cv2.contourArea(c)
#    if len(approx) > 5 and area > 1000 and area < 500000:
    ((x, y), r) = cv2.minEnclosingCircle(c)
    cv2.circle(gray, (int(x), int(y)), int(r), (36, 255, 12), 2)

cv2.imshow('thresh', thresh)
cv2.imshow('opening', opening)

canny = cv2.Canny(gray, 100, 0)
#cv2.imshow('canny', canny)
#gray = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
sobelx = cv2.Sobel(gray,cv2.CV_8U,1,1,ksize=5) 
sobely = cv2.Sobel(gray,cv2.CV_8U,0,1,ksize=5) 
laplacian = cv2.Laplacian(gray,cv2.CV_64F)
laplacian2 = cv2.Laplacian(gray,cv2.CV_8U)
#laplacian3 = cv2.Laplacian(gray,cv2.CV_16S)
#cv2.imshow('sobelx',sobelx)
#cv2.imshow('sobely',sobely)
#cv2.imshow('laplacian',laplacian) 
#cv2.imshow('laplacian2',laplacian2) 
#cv2.imshow('laplacian3',laplacian3) 



#gray = cv2.GaussianBlur(gray, (5, 5), 0)
#cv2.imwrite("gray.jpg", gray)
cv2.imshow("gray", gray)

#edged = cv2.Canny(gray, 110, 220)
#cv2.imwrite("edged.jpg", edged)
#cv2.imshow("edged", edged)

#kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))
#closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
#cv2.imwrite("closed.jpg", closed)
#cv2.imshow("closed", closed)

#cnts = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#cnts = imutils.grab_contours(cnts)
#total = 0

#for c in cnts:
    # аппроксимируем (сглаживаем) контур
#    peri = cv2.arcLength(c, True)
#    approx = cv2.approxPolyDP(c, 0.02 * peri, True)



#cv2.imwrite("output.jpg", image)
#cv2.imshow("image", image)
cv2.waitKey(0)
cv2.destroyAllWindows()
