import cv2

import numpy as np

from matplotlib import pyplot as plt
     

def CannyThreshold(lowThreshold):

    detected_edges = cv2.GaussianBlur(gray,(3,3),0)

    detected_edges = cv2.Canny(detected_edges,lowThreshold,lowThreshold*ratio,apertureSize = kernel_size)

    dst = cv2.bitwise_and(img,img,mask = detected_edges) # just add some colours to edges from original image.

    cv2.imshow('canny demo',dst)
     

lowThreshold = 0

max_lowThreshold = 100

ratio = 3

kernel_size = 3
     

img = cv2.imread('kartoshka.jpg')

blue, green, red = cv2.split(img) # Split the image into its channels
img_gs = cv2.imread('kartoshka.jpg', cv2.IMREAD_GRAYSCALE) # Convert image to grayscale

cv2.imshow('red', red) # Display the red channel in the image
cv2.imshow('blur', blue) # Display the red channel in the image
cv2.imshow('green', green) # Display the red channel in the image
cv2.imshow('gray', img_gs) # Display the grayscale version of image

r, threshold = cv2.threshold(red, 175, 255, cv2.THRESH_BINARY)
cv2.imshow('treshold', threshold)
edges = cv2.Canny(img_gs, 100,200)

# Plot the original image against the edges
plt.subplot(121), plt.imshow(img_gs)
plt.title('Original Gray Scale Image')
plt.subplot(122), plt.imshow(edges)
plt.title('Edge Image')

# Display the two images
plt.show()

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
     

cv2.namedWindow('canny demo')
     

cv2.createTrackbar('Min threshold','canny demo',lowThreshold, max_lowThreshold, CannyThreshold)
     

CannyThreshold(0) # initialization

if cv2.waitKey(0) == 27:

    cv2.destroyAllWindows()



