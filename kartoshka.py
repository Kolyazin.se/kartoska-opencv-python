import cv2

import numpy as np

cap = cv2.VideoCapture(0)

thres = 150
thres_maxval = 255
index = 0
layer = 0
level = 75

contours = 0
hierarchy = 0
threshold = 0
edges = 0

ret, img = cap.read()
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

def updatethres():
    global contours
    global hierarchy
    global threshold
    global edges
    r, threshold = cv2.threshold(img_gray, thres, thres_maxval, cv2.THRESH_BINARY)
    cv2.imshow('th', threshold)
    edges = cv2.Canny(threshold, 0, 100)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    closed = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)
    contours, hierarchy = cv2.findContours(image=closed, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)
    cv2.imshow('edges', edges)
#    cv2.imshow('closed', closed)
    update()

def update_thres(v):
    global thres
    thres = v
    updatethres()

def update_thres_maxval(v):
    global thres_maxval
    thres_maxval = v
    updatethres()

image_copy = 0

def update():
    global image_copy
    # draw contours on the original image
    image_copy = img.copy()
    cv2.drawContours(image=image_copy, contours=contours, contourIdx=index, color=(0, 255, 0), thickness=2, lineType=cv2.LINE_AA)
    for cnt in contours:
        if (len(cnt) > level):
            m = cv2.moments(cnt)
            cx = int(m["m10"] / m["m00"])
            cy = int(m["m01"] / m["m00"])
            cv2.circle(image_copy, (cx, cy), 5, (0, 255, 255), 2)
            print(cx, cy)
    # see the results
    print(' ')
    cv2.imshow('Approximation', image_copy)

def update_index(v):
    global index
    index = v-1
    update()

def update_layer(v):
    global layer
    layer = v
    update()

def update_level(v):
    global level
    level = v
    update()

update_thres(150)
update_thres_maxval(255)
cv2.createTrackbar( "thres", "th", 150, 255, update_thres )
cv2.createTrackbar( "maxval", "th", 255, 255, update_thres_maxval )

update_index(0)
update_layer(0)
update_level(75)
cv2.createTrackbar( "contour", "Approximation", 0, 20, update_index )
cv2.createTrackbar( "layers", "Approximation", 0, 7, update_layer )
cv2.createTrackbar( "level", "Approximation", 75, 255, update_level )

while True:

    ret, img = cap.read()
#    img = cv2.imread('kartoshka.jpg')

#    blue, green, red = cv2.split(img) # Split the image into its channels
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#    img_gs = cv2.imread('kartoshka.jpg', cv2.IMREAD_GRAYSCALE) # Convert image to grayscale
    updatethres()
    update()

    if cv2.waitKey(10) == 27: # Клавиша Esc
        break
#    cv2.waitKey(0)
#cv2.imwrite('contours_none_image1.jpg', image_copy)

cap.release()
cv2.destroyAllWindows()